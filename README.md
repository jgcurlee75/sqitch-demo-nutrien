## Sqitch Demo Nutrien

The intent of this repository is to demonstrate the basic features of sqitch
for managing Postgres database migrations. The hope is to create a few tables,
show example commands to perform DB build up and tear down the database and to
provide examples of common commands used to manage the migrations. Goal is to
produce a packaged tagged bundle developers can gran and experiment with.

---

## Sqitch Documentation 

1. [Sqitch homepage](https://sqitch.org/)
2. [Sqitch Tutorial](https://metacpan.org/pod/sqitchtutorial)
3. [Sqitch GitHub](https://github.com/sqitchers/sqitch)

---

## Starting a New Project to manage database migrations

When starting a new project create a source code repository and associate a unique URI to increase the uniqueness of object identifiers internally.

1. Create a directory for your project (e.g. crop_planning) and cd into it.
2. git init .
3. touch README.md and add high level description of your Data Dictionary and migrations strategy.
4. git commit -m 'Initial project and README.'
5. sqitch init crop_planning --uri <GITHUB_PROJECT_URL> --engine pg

---
